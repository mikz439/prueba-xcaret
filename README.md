# Página prueba de Xcaret
> Página optimizada para producción y base de desarollo.

Template de prueba base realizado con NodeJS, Express, Webpack, EJS, Bootstrap y VueJS.

## Requisitos

* NodeJS >= v8.0
* Disponible el puerto http 8080

## Instalación

Clonar repositorio en la carpeta deseada:

```sh
git clone https://mikz439@bitbucket.org/mikz439/prueba-xcaret.git
```

Entrar a carpeta del proyecto y realizar la instalación de paquetes NPM

```sh
cd prueba-xcaret
npm install
```

Realizar build de archivos optimizados para producción y se levanta el servicio en el puerto 8080

```sh
npm run production
```

## Organización de archivos

```sh
--config    -> Configuraciones dev y produccion de Webpack
--dist      -> Archivos de producción generados
--server    -> Configuración de Express
--src       -> Archivos de desarrollo
    --components    -> Componentes VueJS
    --images        -> Imagenes
    --scss          -> Archivos de estilos Saas
    --views         -> Vistas base sin bundles
    --main.js       -> Script base de carga front
--views     -> Vistas compiladas con bundles generadas por Webpack
--index.js  -> Script base del proyecto para producción
    
```

## Comandos disponibles
 
Modo desarrollo con Webpack con sincronización de navegador usando el Webpack Devserver, se incluye en /config/exmple.js un archivo de prueba parecido al JSON
 
```sh
npm run dev
```
 
Compilación y optimizacón de archivos para producción
 
```sh
npm run build
```
 
Compilación y optimizacón de archivos para producción con regeneración al cambio, usar para probar producción 
 
```sh
npm run build:watch
```
 
Express modo desarrollo, es parecido a **npm run build** pero utilizando Express
 
```sh
npm run dev:express
```

Express modo producción, requiere correr antes **npm run build**
 
```sh
npm run prod
```

Express con Nodemon, para reinicio del servidor al cambio
 
```sh
npm run prod:watch
```

Realiza compilado y optimización y al finalizar corre el servidor en producción
 
```sh
npm run production
```

## Optimizaciones de velocidad

* Minificación de archivos CSS en /config/webpack.prod.js

```sh
/* Extracción de estilos en un sólo CSS con hash para
 que se guarde en cache del navegador y en caso de cambio
 si se refleje el nuevo estilo */
new MiniCSSExtractPlugin({
    filename: '[name].[hash:8].css'
}),
...
/* Plugin de minificación de CSS */
new OptimizeCssAssetsPlugin({
    assetNameRegExp: /\.css$/g,
    cssProcessor: require("cssnano"),
    cssProcessorOptions: {discardComments: {removeAll: true}},
    canPrint: false
}),
```

* Compresión de JS compilado con Uglify

```sh
new UglifyJSPlugin()
```

* Eliminación de CSS no utilizado con PurgeCSS

```sh
/* Busca en los archivos especificados el uso de CSS 
para eliminar el restante */
new PurgecssPlugin({
    paths: glob.sync([
      path.join(__dirname, '../src/index.html'),
      path.join(__dirname, '../**/*.vue'),
      path.join(__dirname, '../src/**/*.js')
    ]),
    whitelist: ['body-app']
}),
```

* Uso de Gzip en Webpack y Express para reducir el tamaño de los archivos creados y poder servirlos

```sh
// /config/webpack.prod.js
new CompressionPlugin({
    algorithm: "gzip"
})

...

// /server/server.js, sirve como estaticos los Gzip
const expressStaticGzip = require("express-static-gzip");
server.use(expressStaticGzip("dist"));
```
