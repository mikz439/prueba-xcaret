const path = require("path")
const webpack = require("webpack")
const HTMLWebpackPlugin = require("html-webpack-plugin")
const OptimizeCssAssetsPlugin = require("optimize-css-assets-webpack-plugin")
const {VueLoaderPlugin} = require("vue-loader")
const isProd = process.env.NODE_ENV === "production"
const MiniCSSExtractPlugin = require("mini-css-extract-plugin")
const PurgecssPlugin = require('purgecss-webpack-plugin')
const CleanWebpackPlugin = require('clean-webpack-plugin')
const UglifyJSPlugin = require("uglifyjs-webpack-plugin")
const CompressionPlugin = require("compression-webpack-plugin")
const glob = require('glob-all');

module.exports = {
    entry: {
        main: ["./src/main.js"]
    },
    mode: "production",
    output: {
        filename: "[name]-bundle.[hash:8].js",
        path: path.resolve(__dirname, "../dist"),
        publicPath: "/"
    },
    devtool: "source-map",
    resolve:  {
        alias: {
            vue$: "vue/dist/vue.esm.js"
        }
    },
    module: {
        rules: [
            {
                test: /\.vue$/,
                use: [{
                    loader: "vue-loader",
                }]
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: "babel-loader"
                    }
                ]
            },
            {
                test: /\.css$/,
                use: [
                    {
                        loader: MiniCSSExtractPlugin.loader
                    },
                    {
                        loader: "css-loader",
                        options: {
                            minimize: true
                        }
                    }
                ]
            },
            {
                test: /.(ttf|otf|eot|svg|woff(2)?)(\?[a-z0-9]+)?$/,
                exclude: /images/,
                use: [{
                    loader: 'file-loader',
                    options: {
                        name: '[name].[ext]',
                        outputPath: 'fonts/',
                        publicPath: '../fonts/'
                    }
                }]
            },
            {
                test: /\.(jpg|svg|png|jpeg)$/,
                use: [
                    {
                        loader: "file-loader",
                        options: {
                            name: "images/[name].[ext]"
                        }
                    }
                ]
            },
            {
                test: /\.(scss)$/,
                use: [
                    MiniCSSExtractPlugin.loader,
                    'css-loader',
                    {
                        // Loader for webpack to process CSS with PostCSS
                        loader: 'postcss-loader',
                        options: {
                            plugins: function () {
                                return [
                                    require('autoprefixer')
                                ];
                            }
                        }
                    },
                    'sass-loader'
                ]
            }
        ]
    },
    plugins: [
        new CleanWebpackPlugin(
            [
                'dist'
            ], {
                root: path.resolve(__dirname, "../")
            }
        ),
        new VueLoaderPlugin(),
        new MiniCSSExtractPlugin({
            filename: '[name].[hash:8].css'
        }),
        new PurgecssPlugin({
            paths: glob.sync([
              path.join(__dirname, '../src/index.html'),
              path.join(__dirname, '../**/*.vue'),
              path.join(__dirname, '../src/**/*.js')
            ]),
            whitelist: ['body-app']
        }),
        new OptimizeCssAssetsPlugin({
            assetNameRegExp: /\.css$/g,
            cssProcessor: require("cssnano"),
            cssProcessorOptions: {discardComments: {removeAll: true}},
            canPrint: false
        }),
        new webpack.DefinePlugin({
            "process.env": {
                NODE_ENV: JSON.stringify("production")
            }
        }),
        new HTMLWebpackPlugin({
            template: '!!raw-loader!' + path.join(__dirname, "../src", "views", "index.ejs"),
            filename: path.join(__dirname, "../views", "index.ejs"),
            inject: true
        }),
        new HTMLWebpackPlugin({
            template: '!!raw-loader!' + path.join(__dirname, "../src", "404.html"),
            filename: path.join(__dirname, "../dist", "404.html"),
            inject: false
        }),
        new HTMLWebpackPlugin({
            template: '!!raw-loader!' + path.join(__dirname, "../src", "500.html"),
            filename: path.join(__dirname, "../dist", "500.html"),
            inject: false
        }),
        new UglifyJSPlugin(),
        new CompressionPlugin({
            algorithm: "gzip"
        })
    ]
}
