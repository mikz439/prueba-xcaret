import express from "express"

const server = express()

import axios from "axios";
import path from "path"

//Obtiene el modo NODE_ENV para su uso
const isProd = process.env.NODE_ENV === "production";
console.log(process.env.NODE_ENV);

//Puerto utilizado por Express, en caso de tener el puerto ocupado puede cambiar este valor
const PORT = 8080;

/* En modo diferente a produccion, se corre Express con Webpack */
if (!isProd) {
    const webpack = require("webpack");
    const config = require("../config/webpack.dev.js");
    const compiler = webpack(config);

    //Middlewares para Express tomando archivos y datos webpack para desarrollo
    const webpackDevMiddleware = require("webpack-dev-middleware")(
        compiler,
        config.devServer
    );

    const webpackHotMiddlware = require("webpack-hot-middleware")(
        compiler,
        config.devServer
    );

    server.use(webpackDevMiddleware);
    server.use(webpackHotMiddlware);

    //Se usa como directorio de archivos estáticos a /dist
    const staticMiddleware = express.static("dist");
    server.use(staticMiddleware)
}else{
    //En modo producción se utiliza como template el EJS con los bundles inyectados
    server.set('view engine', 'ejs');

    /* La ruta raíz en producción sirve la vista generada y mediante Axios llama al
    JSON de Xcaret para generar datos de la página, en caso de error en axios o de que la data recibida
    tenga en el index 0 un objecto, se sirve el error 500 */
    server.get('/', async(req, res) => {

        try{

            let result = await axios.get('https://experienciasxcaret.github.io/Developers/api/front.json')

            if(!!result.data && !!result.data[0]){
                result = result.data[0];

                res.render('index', {result: result});
            }else{
                throw new Error('Los datos de la solicitud ajax no pueden ser procesados en la vista')
            }
        }catch(e){
            console.error(e);

            res.status(500).sendFile(path.join(__dirname, '../dist/500.html'));
        }
    });

    /* Servir los archivos Gzip como estáticos para optimización */
    const expressStaticGzip = require("express-static-gzip");
    server.use(expressStaticGzip("dist"));

    // Si la ruta no es ni la raiz o algún archivo existente, se manda error 404
    server.use(function(req, res) {
        res.status(404).sendFile(path.join(__dirname, '../dist/404.html'));
    });
}

//Se sirve Express en el puerto asignado
server.listen(PORT, () => {
    console.log(
        `Servidor listo en \x1b[32m http://localhost:${PORT} \x1b[0m in  ${process.env.NODE_ENV}`
    )
});
