//Bootstrap 4, incluye jQuery y Popper
import 'bootstrap';

//Archivo de estilos personalizados
import './scss/main.scss';

//Componentes Vue importados
import Vue from 'vue';

import HeaderTop from './components/header_top.vue';        //Header fijado arriba
import Header from './components/header.vue';               //Header con logo y navegación
import Main from './components/main.vue';                   //Sidebar y contenido principal
import FooterTop from './components/footer_top.vue';        //Footer posterior con logos y sitemap
import Footer from './components/footer.vue';               //Footer inferior con telefonos

/* Las imagenes importadas aquí se van a optimizar en Gzip en producción
   para esta prueba no se consideran imagenes generadas dinamicamente  */
require('./images/logo-xcaret-mexico.png');
require('./images/logo_xelha.svg');
require('./images/logo_xenotes.svg');
require('./images/logo_xenses.svg');
require('./images/logo_xichen.svg');
require('./images/logo_xoximilco.svg');
require('./images/logo_xplor.svg');
require('./images/us.svg');
require('./images/msi.png');

/* Componente raiz del sitio, mediante inyección de EJS en el componente se recibe el
prop page-data que pasara datos a los componentes hijos */
Vue.component('app-component', {
    data: function () {
        return {}
    },
    props: ['pageData'],
    template: `<div>
        <HeaderTop />
        <Header />
        <Main 
            :descripcion="pageData.descripcion"
            :cta="pageData.CTA"
            :atractivos="pageData.atractivos"     
            :galeria="pageData.img"     
            :mapa="pageData.maps360"     
            :seccion="pageData.seccion"     
            :titulo="pageData.tituloH1"     
            :video="pageData.videoYT"          
        />
        <FooterTop />
        <Footer />
    </div>`,
    components: {
        HeaderTop,
        Header,
        Main,
        FooterTop,
        Footer
    }
});

/* Instancia Vue en el div#app */
new Vue({
    el: '#app'
});